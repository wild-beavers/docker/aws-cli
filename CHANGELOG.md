## 2.15.57-1

- maintenance: bump AWS cli to `2.15.57-r0`
- tech: bump base Alpine image to `3.20`

## 2.13.25-2

- feat: uses tini as entrypoint

## 2.13.25-1

- maintenance: bump AWS cli to `2.13.25`
- tech: bump base Alpine image to `3.19`
- doc: rewrite the changelog to comply with Wildbeaver standards

## 1.1.0

- maintenance: add back for pins aws-cli, pins to `2.12.6-r0`

## 1.0.1

- maintenance: temporarily removes the pin for aws-cli, because of discrepancy between amd64 and aarch64

## 1.0.0

- feat: (BREAKING) `aws-cli` v2: <https://docs.aws.amazon.com/cli/latest/userguide/cliv2-migration-changes.html>
- maintenance: updates alpine base image `3.18` and aws-cli to `2.12.1-r0`

## 0.3.0

- refactor: renames `Dockerfile` to `Containerfile`
- refactor: updates labels and other to latest practices
- maintenance: updates alpine base image `3.17` and aws-cli to `1.25.97-r0`
- maintenance: updates pre-commit deps + latest practices
- test: uses generic container pipeline

## 0.2.0

- feat: updates alpine image and aws-cli to `1.22.81-r0`
- maintenance: updates pre-commit deps

## 0.1.0

- feat: adds base code with aws-cli

## 0.0.0

- tech: initial version
