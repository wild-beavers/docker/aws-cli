ARG IMAGE="docker.io/library/alpine"
ARG IMAGE_TAG="3.20"

FROM $IMAGE:$IMAGE_TAG

ENV AWS_CLI_VERSION=2.15.57-r0

ARG IMAGE
ARG IMAGE_TAG
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_DESCRIPTION
ARG VCS_URL
ARG VCS_TITLE
ARG VCS_NAMESPACE
ARG CVS_REF
ARG CVS_DESCRIPTION
ARG CVS_URL
ARG CVS_TITLE
ARG CVS_NAMESPACE
ARG LICENSE
ARG VERSION

VOLUME /data

COPY resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["/sbin/tini", "--", "aws"]

LABEL "org.opencontainers.image.created"=${BUILD_DATE} \
      "org.opencontainers.image.authors"="${CVS_NAMESPACE} - https://gitlab.com/wild-beavers/" \
      "org.opencontainers.image.url"="${CVS_URL}" \
      "org.opencontainers.image.documentation"="${CVS_URL}" \
      "org.opencontainers.image.source"="${CVS_URL}" \
      "org.opencontainers.image.version"="${VERSION}" \
      "org.opencontainers.image.vendor"="${CVS_NAMESPACE}" \
      "org.opencontainers.image.licenses"="${LICENSE}" \
      "org.opencontainers.image.title"="${CVS_TITLE}" \
      "org.opencontainers.image.description"="${CVS_DESCRIPTION}" \
      "org.opencontainers.image.base.name"="${IMAGE}:${IMAGE_TAG}" \

      "org.opencontainers.applications.amazon-cli.version"=$AWS_CLI_VERSION
